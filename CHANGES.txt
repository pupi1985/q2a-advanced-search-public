Changelog
=========

# 1.1.0

* Add support to translate search commands

# 1.0.1

* Fix a minor documentation bug
* Add ability to disable the plugin in plugin settings

# 1.0.0

* First release
